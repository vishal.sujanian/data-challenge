# JUNIQE data challenge
Data and BI challenge for the candidates

## Challenge description

You are looking at data from an e-commerce website. The site is very simple and has just 4 pages: 
- The first page is the home page. When you come to the site for the first time, you can only land on the home page as a first page. 
- From the home page, the user can perform a search and land on the search page. 
- From the search page, if the user clicks on a product, she will get to the payment page, where she is asked to provide payment information in order to buy that product. 
- If she does decide to buy, she ends up on the confirmation page.

The company CEO isn't very happy with the volume of sales and, especially, of sales coming from new users. Therefore, she asked you to investigate whether there is something wrong in the conversion funnel or, in general, if you could suggest how the conversion rate can be improved.

Specifically, she is interested in:
- A full picture of the funnel conversion rate for both desktop and mobile
- Some insights on what the product team should focus on in order to improve conversion rate as well as anything you might discover that could help improve

## Dataset
We will be using the E-commerce Website Data Set freely available on the [Kaggle website](https://www.kaggle.com/aerodinamicc/ecommerce-website-funnel-analysis). You can find it as part of the repository in: `data.zip` file.

For every page, the corresponding CSV contains a list of user_id’s that visited that page and a categorical column called a page. The user table contains information about the date the user accessed the website as well as the gender and device used. Data is for visitors to the site from 1/1/2015 - 4/30/2015.

All the tables refer to only the user's ﬁrst experience on-site.

Tables are:
- `user_table` - info about the user
- `home_page_table` - Users who landed on the home page
- `search_page_table` - Users who landed on the search_page
- `payment_page_table` - Users who landed on the payment_page
- `payment_confirmation_table` - Users who landed on the payment_confirmation_table. These are the users who bought the product


## The goal

The goal is to perform funnel analysis for an e-commerce website. 

Typically, websites have a clear path to conversion: for instance, you land on the home page, then you search, select a product, and buy it. At each of these steps, some users will drop off and leave the site. The sequence of pages that lead to conversion is called 'funnel'.

Data and analysis can have a tremendous impact on funnel optimization. Funnel analysis allows to understand where/when our users abandon the website. It gives crucial insights on user behavior and on ways to improve the user experience. Also, it often allows to discover bugs.

## Your task
There are no limitations on the tooling you can use, we expect the result to include the following:
- Detailed explanation and visualization of the findings
- Recommendations to increase the conversion rates
- Recommendations to improve the dataset or data collection
